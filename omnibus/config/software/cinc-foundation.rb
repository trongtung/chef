name "ecsc-foundation"
license "Apache-2.0"
license_file "LICENSE"

# Grab accompanying notice file.
# So that Open4/deep_merge/diff-lcs disclaimers are present in Omnibus LICENSES tree.
license_file "NOTICE"

skip_transitive_dependency_licensing true

if windows?
  source path: "c:/ecsc-project/ecsc"
else
  source path: "/opt/ecsc"
end

relative_path "ecsc-foundation"

build do
  sync "#{project_dir}", "#{install_dir}"
end
