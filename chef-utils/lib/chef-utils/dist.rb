# frozen_string_literal: true
module ChefUtils
  # This class is not fully implemented, depending on it is not recommended!
  module Dist
    class Apply
      # The chef-apply product name
      PRODUCT = "Ecsc Apply"

      # The chef-apply binary
      EXEC = "ecsc-apply"
    end

    class Automate
      # name of the automate product
      PRODUCT = "Ecsc Dashboard"
    end

    class Cli
      # the chef-cli product name
      PRODUCT = "Ecsc CLI"

      # the chef-cli gem
      GEM = "chef-cli"
    end

    class Habitat
      # name of the Habitat product
      PRODUCT = "Biome"

      # A short designation for the product
      SHORT = "biome"

      # The hab cli binary
      EXEC = "bio"
    end

    class Infra
      # When referencing a product directly, like Chef (Now Chef Infra)
      PRODUCT = "Ecsc Client"

      # A short designation for the product, used in Windows event logs
      # and some nomenclature.
      SHORT = "ecsc"

      # The client's alias (chef-client)
      CLIENT = "ecsc-client"

      # The chef executable, as in `chef gem install` or `chef generate cookbook`
      EXEC = "ecsc"

      # The chef-shell executable
      SHELL = "ecsc-shell"

      # Configuration related constants
      # The chef-shell configuration file
      SHELL_CONF = "ecsc_shell.rb"

      # The user's configuration directory
      USER_CONF_DIR = ".ecsc"

      # The suffix for Chef's /etc/chef, /var/chef and C:\\Chef directories
      # "chef" => /etc/ecsc, /var/ecsc, C:\\ecsc
      DIR_SUFFIX = "ecsc"

      # The client's gem
      GEM = "chef"

      # The client's container image
      CONTAINER_REF = "chef/chef"
    end

    class Inspec
      # The InSpec product name
      PRODUCT = "Ecsc Verify"

      # The inspec binary
      EXEC = "ecsc-verify"
    end

    class Org
      # TODO
      # product Website address
      WEBSITE = "https://chef.io"
      
      # TODO
      # The downloads site
      DOWNLOADS_URL = "chef.io/downloads"

      # The legacy conf folder: C:/opscode/chef. Specifically the "opscode" part
      # DIR_SUFFIX is appended to it in code where relevant
      LEGACY_CONF_DIR = "ecsc-project"

      # Enable forcing Chef EULA
      ENFORCE_LICENSE = false

      # product patents page
      PATENTS = "https://www.chef.io/patents"

      # knife documentation page
      KNIFE_DOCS = "https://docs.chef.io/workstation/knife/"

      # the name of the overall infra product
      PRODUCT = "Ecsc"

      # TODO
      # Omnitruck URL
      OMNITRUCK_URL = "https://omnitruck.chef.io/install.sh"
    end

    class Server
      # The name of the server product
      PRODUCT = "Ecsc Server"

      # The server's configuration directory
      CONF_DIR = "/etc/ecsc-server"

      # The servers's alias (chef-server)
      SERVER = "ecsc-server"

      # The server's configuration utility
      SERVER_CTL = "ecsc-server-ctl"

      # The server`s docs URL
      SERVER_DOCS = "https://docs.chef.io/server/"

      # OS user for server
      SYSTEM_USER = "ecsc"
    end

    class Solo
      # Chef-Solo's product name
      PRODUCT = "Ecsc Solo"

      # The chef-solo executable (legacy local mode)
      EXEC = "ecsc-solo"
    end

    class Workstation
      # The full marketing name of the product
      PRODUCT = "Ecsc Workstation"

      # The suffix for Chef Workstation's /opt/chef-workstation or C:\\opscode\chef-workstation
      DIR_SUFFIX = "ecsc-workstation"

      # Workstation banner/help text
      DOCS = "https://docs.chef.io/workstation/"
    end

    class Zero
      # chef-zero executable
      PRODUCT = "Ecsc Zero"

      # The chef-zero executable (local mode)
      EXEC = "ecsc-zero"
    end
  end
end
